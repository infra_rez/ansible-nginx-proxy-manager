Ansible Role - Nginx Proxy Manager
---
# Objective
An Ansible role that sets up an enviroment and starts Nginx Proxy Manager in a docker container.

# Requirements
- Docker
- Docker Compose
- Ansible Galaxy collection:  `community.docker`
